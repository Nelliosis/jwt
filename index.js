const Koa = require('koa');
const Router = require('koa-router');
const hel = require('koa-helmet');
const jsonBody = require('koa-json-body')
const logger = require('koa-logger');
const dotenv = require('dotenv-safe');

dotenv.config();

const { tokenMiddle, genToken } = require('./jwt');
const { isOK } = require('./user')

const app = new Koa();
const router = new Router();
const port = process.env.PORT || 8080;

router.get('/', async (context) => {
    context.status = 200;
    context.body = { message: 'public route accessed' };
})

router.post('/login', async (context) => {
    const req = context.request.body;
    console.log(req)
    if (!isOK(req)) {
        console.log(`Match?: ${!!isOK(req)}`);
        context.status = 401;
        context.body = { message: 'Wrong user or pw' };
        return;
    }
    console.log(`Match?: ${isOK(req)}`);
    context.status = 200;
    process.env['JWT_SECRET'] = genToken(req.name);
    context.body = { token: process.env['JWT_SECRET'] };
})

router.get('/private', tokenMiddle(), async (context) => {
    context.status = 200;
    context.body = { message: 'private route accessed' };
})

//error handler
app.use((context, next) => {
    return next().catch((error => {
        if (error.status === 401) {
            context.status = 401;
            context.body = { message: 'thats not you.' }
        } else {
            throw error;
        }
    }))
})

//middleware
app.use(logger());
app.use(jsonBody())
app.use(hel())
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(port, console.log(`go to http://localhost:${port}`))
