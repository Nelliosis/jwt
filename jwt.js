const token = require('jsonwebtoken');
const kjwt = require('koa-jwt');
const dotenv = require('dotenv-safe');

dotenv.config();

const tsec = process.env['JWT_SECRET'];

const genToken = (user) => {
    return token.sign({
        user,
        iat: Math.floor((Date.now() / 1000) + 3600)
    }, process.env['JWT_SECRET'])
}

const tokenMiddle = () => kjwt({
    secret: process.env['JWT_SECRET']
})


module.exports = {
    genToken,
    tokenMiddle
}