const dummyusers = [
    {
        name: "jun",
        pass: "pw"
    }
]

const isOK = ({ name, pass }) => {

    // dummyusers.find(user => {
    //     console.log(`user.name ${user.name}, name: ${name}`)
    //     console.log(`password: ${user.pass}, password ${pass}`)
    // })

    return !!(dummyusers.find(user =>
        user.name === name && user.pass === pass
    ))
}

module.exports = {
    isOK
}